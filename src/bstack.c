#include <stdlib.h>
#include <assert.h>
#include "bstack.h"
#include "mpool.h"

typedef struct bstack_item_t bstack_item_t;
struct bstack_item_t
{
    bstack_item_t* prev;
};

struct bstack_t
{
    mpool_t*        mpool;
    size_t          data_size;
    size_t          num_items;
    bstack_item_t*  head;
    int             id;
};


static void* item2ptr(bstack_item_t* item);


void* item2ptr(bstack_item_t* item)
{
    return (void*)((char*)item + sizeof(bstack_item_t));
}


bstack_t* bstack_create(size_t data_size)
{
    static int id = 0;
    bstack_t* s = (bstack_t*)calloc(1, sizeof(bstack_t));
    if (!s)
        return NULL;

    s->id = id++;
    s->mpool = mpool_create(sizeof(bstack_item_t) + data_size);
    if (!s->mpool)
    {
        free(s);
        return NULL;
    }

    s->data_size = data_size;
    s->num_items = 0;
    s->head = NULL;

    return s;
}

void bstack_destroy(bstack_t* s)
{
    assert(s);

    bstack_clear(s);

    mpool_destroy(s->mpool);
    free(s);
}

size_t bstack_size(bstack_t* s)
{
    assert(s);

    return s->num_items;
}

void bstack_clear(bstack_t* s)
{
    assert(s);

    while (s->num_items)
        bstack_pop(s);
}

void* bstack_push(bstack_t* s)
{
    assert(s);

    bstack_item_t* i = (bstack_item_t*)mpool_alloc(s->mpool);

    i->prev = s->head;
    s->head = i;
    s->num_items++;

    return item2ptr(i);
}


void* bstack_last(bstack_t* s)
{
    assert(s);

    if (!s->head)
        return NULL;

    return item2ptr(s->head);
}


int bstack_pop(bstack_t* s)
{
    assert(s);

    bstack_item_t* i = s->head;

    if (!i)
        return 0;

    s->num_items--;
    s->head = i->prev;
    mpool_free(s->mpool, i);

    return 1;
}
