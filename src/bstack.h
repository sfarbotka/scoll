#ifndef STACK_H_
#define STACK_H_

#include <stdio.h>

typedef struct bstack_t bstack_t;

bstack_t* bstack_create(size_t data_size);
void bstack_destroy(bstack_t* s);

size_t bstack_size(bstack_t* s);

void bstack_clear(bstack_t* s);
void* bstack_push(bstack_t* s);
void* bstack_last(bstack_t* s);
int bstack_pop(bstack_t* s);

#endif /* STACK_H_ */
