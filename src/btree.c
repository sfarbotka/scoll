#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>
#include <string.h>
#include "btree.h"
#include "mpool.h"
#include "bstack.h"
#include "debug.h"

#define STACK_BUF_SIZE 15

typedef struct item_t
{
    btree_node_t* node;
    int val;
} item_t;

typedef struct stack_item
{
    size_t count;
    item_t buf[STACK_BUF_SIZE];
} stack_item_t;

struct btree_node_t
{
    int             level;
    btree_node_t*   left;
    btree_node_t*   right;
};

struct btree_t
{
    mpool_t*        mpool;
    bstack_t*       stack;
    stack_item_t*   stack_head; // is used to increase speed
    size_t          data_size;
    size_t          key_size;
    btree_node_t*   ins;
    btree_node_t*   root;
    btree_cmpfunc_t cmp;
    size_t          num_entries;
    int             id;

#ifdef PROFILING
    size_t          search_iters;
#endif
};


static btree_node_t* create_node(btree_t* t, const void* key);
static inline void* node2ptr(btree_t* t, btree_node_t* node);
static inline void* node2key(btree_t* t, btree_node_t* node);
static btree_node_t* aa_skew(btree_node_t* n);
static btree_node_t* aa_split(btree_node_t* n);

static int stack_last(btree_t* t, btree_node_t** node, int* val);
static void stack_last_set_val(btree_t* t, int val);
static void stack_push(btree_t* t, btree_node_t* node, int val);
static void stack_pop(btree_t* t);
static void stack_clear(btree_t* t);

void* node2ptr(btree_t* t, btree_node_t* node)
{
    return (void*)((char*)node + sizeof(btree_node_t) + t->key_size);
}


void* node2key(btree_t* t, btree_node_t* node)
{
    return (void*)((char*)node + sizeof(btree_node_t));
}


int stack_last(btree_t* t, btree_node_t** node, int* val)
{
    stack_item_t* p = t->stack_head;

    if (!p)
        return 0;

    item_t* i = &p->buf[p->count - 1];

    *node = i->node;
    *val = i->val;

    return 1;
}


void stack_last_set_val(btree_t* t, int val)
{
    stack_item_t* p = t->stack_head;

    p->buf[p->count - 1].val = val;
}


void stack_push(btree_t* t, btree_node_t* node, int val)
{
    stack_item_t* p = t->stack_head;

    if (!p || (p->count == STACK_BUF_SIZE))
    {
        p = (stack_item_t*)bstack_push(t->stack);
        p->count = 0;
        t->stack_head = p;
    }

    item_t* i = &p->buf[p->count++];
    i->node = node;
    i->val = val;
}


void stack_pop(btree_t* t)
{
    stack_item_t* p = t->stack_head;

    if (p && (--p->count == 0))
    {
        bstack_pop(t->stack);
        t->stack_head = (stack_item_t*)bstack_last(t->stack);
    }
}


void stack_clear(btree_t* t)
{
    bstack_clear(t->stack);
    t->stack_head = NULL;
}


btree_node_t* create_node(btree_t* t, const void* key)
{
    btree_node_t *n = (btree_node_t*)mpool_alloc(t->mpool);
    if (!n)
        return NULL;

    memcpy(node2key(t, n), key, t->key_size);
    n->level = 1;
    n->left = NULL;
    n->right = NULL;

    return n;
}

btree_t* btree_create(size_t key_size, size_t data_size)
{
    static int id = 0;

    btree_t *t = (btree_t*)calloc(1, sizeof(btree_t));
    if (!t)
        return NULL;
    t->id = id++;

    t->mpool = mpool_create(sizeof(btree_node_t) + key_size + data_size);
    if (!t->mpool)
    {
        free(t);
        return NULL;
    }

    t->stack = bstack_create(sizeof(stack_item_t));
    if (!t->stack)
    {
        mpool_destroy(t->mpool);
        free(t);
        return NULL;
    }

    t->root = NULL;
    t->num_entries = 0;
    t->data_size = data_size;
    t->key_size = key_size;
    t->stack_head = NULL;

    debug_log("btree-%02x: btree_create() ptr=%p\n", t->id, t);

    return t;
}

static void delete_aatree_(btree_t* t, btree_node_t* n)
{
    if (n->left)
        delete_aatree_(t, n->left);
    if (n->right)
        delete_aatree_(t, n->right);
    mpool_free(t->mpool, n);
}

void btree_destroy(btree_t* t)
{
    assert(t);

#ifdef PROFILING
    fprintf(stderr, "btree-%02x: PROFILING DATA:\n", t->id);
    fprintf(stderr, "btree-%02x: search/insert iters count: %zu\n", t->id,
            t->search_iters);

    fprintf(stderr, "btree-%02x: PROFILING DATA ENDED\n", t->id);

#endif
    if (t->root)
        delete_aatree_(t, t->root);
    mpool_destroy(t->mpool);
    free(t);

    debug_log("btree-%02x: btree_destroy()\n", t->id);
}

btree_node_t* aa_skew(btree_node_t* n)
{
    assert(n);
    if (!n->left)
        return n;

    if (n->level != n->left->level)
        return n;
    btree_node_t *left = n->left;
    n->left = left->right;
    left->right = n;
    n = left;
    return n;
}

btree_node_t* aa_split(btree_node_t* n)
{
    assert(n);
    if (!n->right || !n->right->right)
        return n;

    if (n->right->right->level != n->level)
        return n;
    btree_node_t *right = n->right;
    n->right = right->left;
    right->left = n;
    n = right;
    n->level++;
    return n;
}

static btree_node_t* aa_search_(btree_t* t, const void* key, btree_cmpfunc_t cmpf)
{
    btree_node_t* node = t->root;
    int cmp;

    while (node)
    {
#ifdef PROFILING
    t->search_iters++;
#endif
        cmp = cmpf(key, node2key(t, node));
        if (cmp == 0)
            break;

        node = (cmp < 0) ? node->left : node->right;
    }

    return node;
}

static btree_node_t* aa_insert_(btree_t* t, btree_node_t* n, const void* key,
        btree_cmpfunc_t cmpf, int* insert)
{
    btree_node_t* node;
    btree_node_t* child = 0;
    btree_node_t* ret = 0;
    int cmp;

    stack_push(t, t->root, 0);

    while (stack_last(t, &node, &cmp))
    {
#ifdef PROFILING
        t->search_iters++;
#endif
        if (!ret)
        {
            if (!node)
            {
                stack_pop(t);
                ret = create_node(t, key);
                t->num_entries++;
                *insert = 1;
                child = ret;
                continue;
            }

            cmp = cmpf(key, node2key(t, node));
            if (cmp == 0)
            {
                ret = node;
                *insert = 0;
                stack_clear(t);
                child = t->root;
                break;
            }

            stack_last_set_val(t, cmp);

            if (cmp < 0)
                stack_push(t, node->left, 0);
            else
                stack_push(t, node->right, 0);
        }
        else
        {
            if (cmp < 0)
                node->left = child;
            else
                node->right = child;

            stack_pop(t);
            child = aa_skew(node);
            child = aa_split(child);
        }
    }
    t->root = child;

    return ret;
}

void* btree_search(btree_t* t, const void* key, btree_cmpfunc_t cmp, int* insert)
{
    btree_node_t* ret;

    if (*insert)
    {
        ret = aa_insert_(t, NULL, key, cmp, insert);
    }
    else
        ret = aa_search_(t, key, cmp);

    return node2ptr(t, ret);
}

size_t btree_num_nodes(btree_t* t)
{
    return t->num_entries;
}

void btree_foreach(btree_t* t, btree_callback_t callback, void* user)
{
    btree_node_t* node;
    int post;

    if (!t->root)
        return;

    stack_push(t, t->root, 0);

    while(stack_last(t, &node, &post))
    {
        if (!post && (node->left || node->right))
        {
            stack_last_set_val(t, 1);
            if (node->right)
                stack_push(t, node->right, 0);
            if (node->left)
                stack_push(t, node->left, 0);
            continue;
        }

        stack_pop(t);
        callback(t, node2key(t, node), node2ptr(t, node), user);
    }
}
