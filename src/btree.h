#ifndef BTREE_H_
#define BTREE_H_

#include <stdio.h>

typedef struct btree_node_t btree_node_t;
typedef struct btree_t btree_t;

typedef int (*btree_cmpfunc_t)(const void* k1, const void* k2);
typedef void (*btree_callback_t)(btree_t* t, const void* key, void* data, void* user);

size_t btree_num_nodes(btree_t* t);
btree_t* btree_create(size_t key_size, size_t data_size);
void btree_destroy(btree_t* t);
void* btree_search(btree_t* t, const void* key, btree_cmpfunc_t cmp, int* insert);
void btree_foreach(btree_t* t, btree_callback_t callback, void* user);
void btree_set_weight(btree_t* t, void* p, int weight);

#endif /* AATREE_H_ */
