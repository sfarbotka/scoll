//
//  capture.c
//  scoll
//
//  Created by Sergey Farbotka on 9/24/14.
//  Copyright (c) 2014 Sergey Farbotka. All rights reserved.
//

#include <pcap/pcap.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "capture.h"
#include "debug.h"

#define SNAP_LEN 100
#define SIZE_ETHERNET 14


struct hdr_eth
{
    u_char  ether_dhost[6];
    u_char  ether_shost[6];
    u_short ether_type;
};

struct hdr_ip
{
    u_char  ip_vhl;
    u_char  ip_tos;
    u_short ip_len;
    u_short ip_id;
    u_short ip_off;
    u_char  ip_ttl;
    u_char  ip_proto;
    u_short ip_sum;
    struct  in_addr ip_src;
    struct  in_addr ip_dst;
};
#define IP_HL(ip) (((ip)->ip_vhl) & 0x0f)
#define IP_V(ip)  (((ip)->ip_vhl) >> 4)

/* TCP header */
typedef u_int tcp_seq;

struct hdr_tcp
{
    u_short th_sport;
    u_short th_dport;
    tcp_seq th_seq;
    tcp_seq th_ack;
    u_char  th_offx2;
    u_char  th_flags;
    u_short th_win;
    u_short th_sum;
    u_short th_urp;
};
#define TH_OFF(th)      (((th)->th_offx2 & 0xf0) >> 4)

struct hdr_udp
{
    u_short uh_sport;
    u_short uh_dport;
    u_short uh_len;
    u_short uh_sum;
};


struct capture
{
    capture_type_t     type;
    capture_flags_t    flags;
    pcap_t*            pcap;
    struct bpf_program bpf;
    stats_t*           stats;
    char*              dev;

    int                id;
};

static void got_packet(capture_t* c, const struct pcap_pkthdr *header, const u_char *packet);
static long long ts2us(struct timeval ts);
static void file_loop(capture_t* c);


long long ts2us(struct timeval ts)
{
    return ts.tv_sec * 1000000 + ts.tv_usec;
}


void file_loop(capture_t* c)
{
    struct pcap_pkthdr *header;
    const u_char *packet;
    int ret;
    int first = 1;
    long long tprev, t;
    clock_t cl;
    int nowait = c->flags & CAPTURE_FLAGS_NOWAIT;

    debug_log("capture-%02x: file_loop() start\n", c->id);
    do
    {
        ret = pcap_next_ex(c->pcap, &header, &packet);
        if (ret > 0)
        {
            if (!nowait)
            {
                t = ts2us(header->ts);
                if (!first)
                {
                    long long td = t - tprev;

                    cl = clock() - cl;
                    td -= (long long)(cl / (CLOCKS_PER_SEC / 1000000.0f));

                    if (td < 0)
                        td = 0;

                    sleep((unsigned int)(td / 1000000));
                    usleep((unsigned int)(td % 1000000));
                }
                else
                    first = 0;
                cl = clock();
            }

            got_packet(c, header, packet);

            if (!nowait)
                tprev = t;
        }
    }while(ret > 0);
    debug_log("capture-%02x: file_loop() finished\n", c->id);
}


void got_packet(capture_t* c, const struct pcap_pkthdr *header, const u_char *packet)
{
    //const struct hdr_ethernet *ethernet;
    const struct hdr_ip *ip;
    const struct hdr_tcp *tcp;
    const struct hdr_udp *udp;
    
    struct stats_record srec;
    
    int size_ip;
    int size_left;
    
    memset(&srec, 0, sizeof(struct stats_record));
    
    size_left = header->caplen;
    if (size_left < SIZE_ETHERNET)
        return;
    
    //ethernet = (struct hdr_ethernet*)(packet);
    size_left -= SIZE_ETHERNET;
    
    if (size_left < sizeof(struct hdr_ip))
        return;
    
    ip = (struct hdr_ip*)(packet + SIZE_ETHERNET);
    size_ip = IP_HL(ip)*4;
    if (size_ip < 20)
        return;

    size_left -= size_ip;
    
    srec.num_bytes = ntohs(ip->ip_len);
    srec.k.src_ip = ip->ip_src;
    srec.k.dst_ip = ip->ip_dst;
    srec.k.proto = ip->ip_proto;
    
    switch(ip->ip_proto)
    {
        case IPPROTO_TCP:
            if (size_left >= sizeof(struct hdr_tcp))
            {
                tcp = (struct hdr_tcp*)(packet + SIZE_ETHERNET + size_ip);
                srec.k.src_port = ntohs(tcp->th_sport);
                srec.k.dst_port = ntohs(tcp->th_dport);
            }
            break;
        case IPPROTO_UDP:
            if (size_left >= sizeof(struct hdr_udp))
            {
                udp = (struct hdr_udp*)(packet + SIZE_ETHERNET + size_ip);
                srec.k.src_port = ntohs(udp->uh_sport);
                srec.k.dst_port = ntohs(udp->uh_dport);
            }
            break;
    }

    stats_queue(c->stats, &srec);
}

capture_t* capture_create(capture_type_t type, const char* target,
        stats_t* stats, capture_flags_t flags)
{
    static int capture_index = 0;

    capture_t* c;
    char errbuf[PCAP_ERRBUF_SIZE];
    
    const char* filter_exp = "ip";
    bpf_u_int32 net;
    
    
    c = (capture_t*)calloc(1, sizeof(capture_t));
    c->id = capture_index++;
    c->type = type;
    c->flags = flags;
    c->dev = strdup(target);
    c->stats = stats;
    
    if (type == CAPTURE_IFACE)
    {
        bpf_u_int32 mask;
        if (pcap_lookupnet(c->dev, &net, &mask, errbuf) == -1)
        {
            fprintf(stderr, "Couldn't get netmask for device %s: %s\n",
                    c->dev, errbuf);
            net = PCAP_NETMASK_UNKNOWN;
        }

        printf("Device: %s\n", c->dev);

        c->pcap = pcap_open_live(c->dev, SNAP_LEN, 1, 1000, errbuf);
    }
    else if (type == CAPTURE_FILE)
    {
        printf("File: %s\n", c->dev);
        net = PCAP_NETMASK_UNKNOWN;
        c->pcap = pcap_open_offline(c->dev, errbuf);
    }

    if (!c->pcap)
    {
        fprintf(stderr, "Couldn't open device %s: %s\n", c->dev, errbuf);
        goto fail;
    }
    
    if (pcap_datalink(c->pcap) != DLT_EN10MB)
    {
        fprintf(stderr, "%s is not an Ethernet\n", c->dev);
        goto fail;
    }
    
    if (pcap_compile(c->pcap, &c->bpf, filter_exp, 0, net) == -1)
    {
        fprintf(stderr, "Couldn't parse filter %s: %s\n",
                filter_exp, pcap_geterr(c->pcap));
        goto fail;
    }
    
    if (pcap_setfilter(c->pcap, &c->bpf) == -1)
    {
        fprintf(stderr, "Couldn't install filter %s: %s\n",
                filter_exp, pcap_geterr(c->pcap));
        goto fail;
    }

    debug_log("capture-%02x: capture_create(), ptr=%p.\n", c->id, c);

    return c;

fail:
    free(c->dev);
    free(c);
    return NULL;
}


void capture_start(capture_t* c)
{
    debug_log("capture-%02x: capture_start().\n", c->id);

    if (c->type == CAPTURE_IFACE)
        pcap_loop(c->pcap, -1, (pcap_handler)got_packet, (u_char*)c);
    else
        file_loop(c);
}

capture_type_t capture_type(capture_t *c)
{
    return c->type;
}


void capture_stop(capture_t* c)
{
    debug_log("capture-%02x: capture_stop().\n", c->id);
    pcap_breakloop(c->pcap);
}

void capture_destroy(capture_t* c)
{
    debug_log("capture-%02x: capture_destroy().\n", c->id);
    
    free(c->dev);
    pcap_freecode(&c->bpf);
    pcap_close(c->pcap);
    
}

