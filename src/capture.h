//
//  capture.h
//  scoll
//
//  Created by Sergey Farbotka on 9/24/14.
//  Copyright (c) 2014 Sergey Farbotka. All rights reserved.
//

#ifndef scoll_capture_h
#define scoll_capture_h

#include "stats.h"

enum
{
    CAPTURE_FILE,
    CAPTURE_IFACE
};
typedef int capture_type_t;

enum
{
    CAPTURE_FLAGS_NONE = 0,
    CAPTURE_FLAGS_NOWAIT = 1,
};
typedef int capture_flags_t;


typedef struct capture capture_t;

capture_t* capture_create(capture_type_t type, const char* target,
		stats_t* stats, capture_flags_t flags);
void capture_start(capture_t* c);
capture_type_t capture_type(capture_t *c);
void capture_stop();
void capture_destroy(capture_t* c);

#endif
