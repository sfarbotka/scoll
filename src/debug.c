/*
 * debug.c
 *
 *  Created on: Sep 30, 2014
 *      Author: fsv
 */

#include "debug.h"

#ifdef PROFILING

#define NORMALIZED_COUNT 100.0f

static clock_t norm_clock(clock_t clock, size_t count);
static float clock2ms(clock_t cl);

static char profiling_buf[0x1000];

clock_t norm_clock(clock_t clock, size_t count)
{
    if (!count)
        return 0;
    return (clock_t)(clock * NORMALIZED_COUNT / (float)count);
}

float clock2ms(clock_t cl)
{
    return cl / (CLOCKS_PER_SEC / 1000.0f);
}

const char* profiling2str(const char* name, clock_t cl, size_t cnt,
        size_t norm_cnt)
{
    float ms = clock2ms(cl);
    float nms =clock2ms(norm_clock(cl, norm_cnt));
    snprintf(profiling_buf, 0x1000-1, "%s %.3fms, %zu times, %.3fms (norm)",
            name, ms, cnt, nms);

    return profiling_buf;
}

#endif

