//
//  debug.h
//  scoll
//
//  Created by Sergey Farbotka on 9/25/14.
//  Copyright (c) 2014 Sergey Farbotka. All rights reserved.
//

#ifndef DEBUG_H_
#define DEBUG_H_

#include <stdio.h>
#include <time.h>

#ifdef DEBUG_OUTPUT
#define debug_log(...) fprintf (stderr, ##__VA_ARGS__)
#else
#define debug_log(...)
#endif

#ifdef PROFILING

const char* profiling2str(const char* name, clock_t clock, size_t count,
        size_t norm_count);

#endif

#endif // DEBUG_H_
