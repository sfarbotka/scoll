/*
 * mpool.c
 *
 *  Created on: Oct 3, 2014
 *      Author: fsv
 */

#include <stdlib.h>
#include "mpool.h"
#include <assert.h>
#include "debug.h"

#define HEAP_SIZE_MULTIPLIER 1.3
#define HEAP_SIZE_MIN 100

typedef struct mpool_block
{
    struct mpool_block* prev;
    struct mpool_block* next;
    int allocated;
    // data block here
} mpool_block_t;

typedef struct mpool_heap
{
    size_t bytes_count;
    size_t blocks_count;
    size_t block_size;
    mpool_block_t* bhead;
    mpool_block_t* btail;
    struct mpool_heap* prev;
    struct mpool_heap* next;
} mpool_heap_t;

struct mpool
{
    size_t          data_size;
    size_t          block_size;
    size_t          blocks_total_count;
    mpool_heap_t*   heap_head;
    mpool_block_t*  bhead_free;
    mpool_block_t*  bhead_alloc;

    int             id;

#ifdef PROFILING
    clock_t         alloc_clock;
    size_t          alloc_count;
    clock_t         free_clock;
    size_t          free_count;

    clock_t         heap_add_clock;
    size_t          heap_add_count;

    size_t          allocated_cnt;
    size_t          allocated_max;
#endif
};

static mpool_block_t* ptr2block(void* p);
static void* block2ptr(mpool_block_t* block);

static mpool_heap_t* heap_create(size_t block_size, size_t nblocks);
static void heap_destroy(mpool_heap_t* heap);
static void heap_add(mpool_t* pool);
static mpool_block_t* heap_next_block(mpool_heap_t* heap, mpool_block_t* block);
static mpool_block_t* heap_end_block(mpool_heap_t* heap);


mpool_block_t* ptr2block(void* ptr)
{
    return (mpool_block_t*)((char*)ptr - sizeof(mpool_block_t));
}

void* block2ptr(mpool_block_t* block)
{
    return (void*)((char*)block + sizeof(mpool_block_t));
}


mpool_heap_t* heap_create(size_t block_size, size_t nblocks)
{
    size_t bytes_count = block_size * nblocks;
    mpool_heap_t* heap = (mpool_heap_t*)malloc(
            sizeof(mpool_heap_t) + bytes_count);
    heap->block_size = block_size;
    heap->bytes_count = bytes_count;
    heap->blocks_count = nblocks;
    heap->next = NULL;
    heap->prev = NULL;
    heap->bhead = (mpool_block_t*)((char*)heap + sizeof(mpool_heap_t));

    mpool_block_t* prev = 0;
    mpool_block_t* cur = heap->bhead;
    mpool_block_t* end = heap_end_block(heap);
    for (; cur < end; prev = cur, cur = heap_next_block(heap, cur))
    {
        if (prev)
            prev->next = cur;
        cur->prev = prev;
        cur->allocated = 0;
    }
    prev->next = 0;

    heap->btail = prev;

    return heap;
}

void heap_destroy(mpool_heap_t* heap)
{
#ifndef NDEBUG
    mpool_block_t* cur = heap->bhead;
    mpool_block_t* end = heap_end_block(heap);
    for (; cur < end; cur = heap_next_block(heap, cur))
        assert(cur->allocated == 0);

    // TODO check cur->prev->next == cur
    // TODO check cur->next->prev == cur
#endif

    free(heap);
}


void heap_add(mpool_t* pool)
{
//#ifdef PROFILING
//    clock_t c = clock();
//#endif

    size_t nblocks = (size_t)(pool->blocks_total_count * HEAP_SIZE_MULTIPLIER);

    mpool_heap_t* heap = heap_create(pool->block_size, nblocks);

    // add heap
    heap->prev = NULL;
    heap->next = pool->heap_head;
    pool->heap_head->prev = heap;
    pool->heap_head = heap;
    pool->blocks_total_count += heap->blocks_count;

    // add to free blocks
    if (pool->bhead_free)
    {
        heap->btail->next = pool->bhead_free;
        pool->bhead_free->prev = heap->btail;
    }
    pool->bhead_free = heap->bhead;

//#ifdef PROFILING
//    c = clock() - c;
//    pool->heap_add_clock += c;
//    pool->heap_add_count++;
//#endif
}


mpool_block_t* heap_next_block(mpool_heap_t* heap, mpool_block_t* block)
{
    return (mpool_block_t*)((char*)block + heap->block_size);
}


mpool_block_t* heap_end_block(mpool_heap_t* heap)
{
    return (mpool_block_t*)((char*)heap->bhead + heap->bytes_count);
}


mpool_t* mpool_create(size_t data_size)
{
    static int id = 0;

    if (!data_size)
        return 0;

    mpool_t* pool = (mpool_t*)calloc(1, sizeof(mpool_t));
    pool->id = id++;
    pool->data_size = data_size;
    pool->block_size = data_size + sizeof(mpool_block_t);

    mpool_heap_t* heap = heap_create(pool->block_size, HEAP_SIZE_MIN);
    pool->heap_head = heap;
    pool->bhead_free = heap->bhead;
    pool->bhead_alloc = NULL;
    pool->blocks_total_count = heap->blocks_count;

    debug_log("mpool-%02x: mpool_create(), ptr=%p.\n", pool->id, pool);

    return pool;
}

void mpool_destroy(mpool_t* pool)
{
    assert(pool->bhead_alloc == NULL);

//#ifdef PROFILING
//    fprintf(stderr, "mpool-%02x: PROFILING DATA:\n", pool->id);
//
//    fprintf(stderr, "mpool-%02x: allocated_max: %zu\n", pool->id,
//            pool->allocated_max);
//    fprintf(stderr, "mpool-%02x: %s\n", pool->id,
//            profiling2str("mpool_alloc()", pool->alloc_clock,
//                    pool->alloc_count, pool->alloc_count));
//    fprintf(stderr, "mpool-%02x: %s\n", pool->id,
//            profiling2str("mpool_free()", pool->free_clock,
//                    pool->free_count, pool->free_count));
//    fprintf(stderr, "mpool-%02x: %s\n", pool->id,
//            profiling2str("heap_add()", pool->heap_add_clock,
//                    pool->heap_add_count, pool->heap_add_count));
//
//    fprintf(stderr, "mpool-%02x: PROFILING DATA ENDED\n", pool->id);
//#endif

    mpool_heap_t* cur = pool->heap_head;
    mpool_heap_t* next;
    for (;cur; cur = next)
    {
        next = cur->next;
        heap_destroy(cur);
    }

    free(pool);

    debug_log("mpool-%02x: mpool_destroy()\n", pool->id);
}


void* mpool_alloc(mpool_t* pool)
{
//#ifdef PROFILING
//    clock_t c = clock();
//#endif

    if (!pool->bhead_free)
        heap_add(pool);

    // pop from free list
    mpool_block_t* b = pool->bhead_free;

    assert(!b->allocated);

    pool->bhead_free = b->next;
    if (pool->bhead_free)
        pool->bhead_free->prev = 0;

    // push to alloc list
    if (pool->bhead_alloc)
        pool->bhead_alloc->prev = b;
    b->next = pool->bhead_alloc;
    pool->bhead_alloc = b;

    b->allocated = 1;

//#ifdef PROFILING
//    c = clock() - c;
//    pool->alloc_clock += c;
//    pool->alloc_count++;
//    pool->allocated_cnt++;
//    if (pool->allocated_max < pool->allocated_cnt)
//        pool->allocated_max = pool->allocated_cnt;
//#endif

    return block2ptr(b);
}

void mpool_free(mpool_t* pool, void* ptr)
{
    mpool_block_t* b = ptr2block(ptr);

    assert(b->allocated);

//#ifdef PROFILING
//    clock_t c = clock();
//#endif

    b->allocated = 0;

    // remove from alloc list
    if (b->prev)
        b->prev->next = b->next;
    if (b->next)
        b->next->prev = b->prev;
    if (pool->bhead_alloc == b)
        pool->bhead_alloc = b->next;

    // push to free list
    b->prev = NULL;
    if (pool->bhead_free)
        pool->bhead_free->prev = b;
    b->next = pool->bhead_free;
    pool->bhead_free = b;

//#ifdef PROFILING
//    c = clock() - c;
//    pool->free_clock += c;
//    pool->free_count++;
//    pool->allocated_cnt--;
//#endif

}
