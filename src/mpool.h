/*
 * mpool.h
 *
 *  Created on: Oct 3, 2014
 *      Author: fsv
 */

#ifndef MPOOL_H_
#define MPOOL_H_

#include <stdio.h>

typedef struct mpool mpool_t;

mpool_t* mpool_create(size_t data_size);
void mpool_destroy(mpool_t* pool);

void* mpool_alloc(mpool_t* pool);
void mpool_free(mpool_t* pool, void* ptr);

#endif /* MPOOL_H_ */
