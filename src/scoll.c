#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include "capture.h"
#include "stats.h"
#include "timer.h"

#define STATS_SAVE_INTERVAL (60)

enum
{
    STATS_NONE,
    STATS_SAVE,
    STATS_EXIT
};


static pthread_t stats_thread;
static pthread_mutex_t stats_mutex;
static pthread_cond_t stats_cond;
static int stats_val;

stats_t* g_stats;
capture_t* g_capture;

static pthread_t capture_thread;

static void finish_stats();
static void print_app_usage(void);
static void* stats_func(stats_t* s);
static void* capture_func(capture_t* c);
static void set_stats_val(int value);
static void timer_handler();
static void sig_handler(int sig);
static int sig_setup();
static int create_stats_thread(stats_t* s);
static void create_capture_thread(capture_t* c);


void print_app_usage(void)
{
    printf("Usage: scoll <--file|--iface> <target> <outdir>\n");
    printf("\n");
    printf("Options:\n");
    printf("    --file       Open file <target>\n");
    printf("    --iface      Open interface <target>\n");
    printf("    target       Listen on <target> (either file or interface)\n"
           "                 for packets.\n");
    printf("    outdir       Directory for stats files.\n");
    printf("\n");

    return;
}

void* stats_func(stats_t* s)
{
    fprintf(stderr, "stats_func() thread started.\n");
    
    pthread_mutex_lock(&stats_mutex);
    while (stats_val != STATS_EXIT)
    {
        pthread_cond_wait(&stats_cond, &stats_mutex);
        if (stats_val != STATS_NONE)
            stats_save(s);
        
        if (stats_val != STATS_EXIT)
        {
            stats_val = STATS_NONE;
            timer_arm_once(STATS_SAVE_INTERVAL);
        }

    }
    pthread_mutex_unlock(&stats_mutex);

    fprintf(stderr, "stats_func() thread finished.\n");
    pthread_exit(NULL);
}


void* capture_func(capture_t* c)
{
    fprintf(stderr, "capture_func() thread started.\n");
    capture_start(c);

    finish_stats();

    fprintf(stderr, "capture_func() thread finished.\n");
    pthread_exit(NULL);
}


void set_stats_val(int value)
{
    pthread_mutex_lock(&stats_mutex);
    stats_val = value;
    pthread_cond_signal(&stats_cond);
    pthread_mutex_unlock(&stats_mutex);
}


void timer_handler()
{
    set_stats_val(STATS_SAVE);
}

void finish_stats()
{
    stats_stop(g_stats);
}

void sig_handler(int sig)
{
    fprintf(stderr, "Signal received. Finishing...\n");
    
    capture_stop(g_capture);
}

int sig_setup()
{
    struct sigaction act;
    
    bzero(&act, sizeof(struct sigaction));
    act.sa_handler=&sig_handler;
    
    if (0 != sigaction(SIGTERM, &act, NULL))
    {
        fprintf(stderr, "sigaction(SIGTERM) failed\n");
        return 0;
    }

    if (0 != sigaction(SIGINT, &act, NULL))
    {
        fprintf(stderr, "sigaction(SIGINT) failed\n");
        return 0;
    }

    return 1;
}

int create_stats_thread(stats_t* s)
{
    pthread_attr_t thread_attr;

    fprintf(stderr, "Starting stats thread.\n");

    // Initialize
    pthread_mutex_init(&stats_mutex, NULL);
    pthread_cond_init(&stats_cond, NULL);
    stats_val = STATS_NONE;
    
    if (!timer_setup(timer_handler))
    {
        fprintf(stderr, "Unable to setup timer.\n");
        return 0;
    }
    
    // Create stats thread
    pthread_attr_init(&thread_attr);
    pthread_attr_setdetachstate(&thread_attr, PTHREAD_CREATE_JOINABLE);
    pthread_create(&stats_thread, &thread_attr, (void *(*)(void *))stats_func, s);
    
    timer_arm_once(STATS_SAVE_INTERVAL);
    
    return 1;
}

void create_capture_thread(capture_t* c)
{
    pthread_attr_t thread_attr;
    
    fprintf(stderr, "Starting capture thread.\n");
    
    pthread_attr_init(&thread_attr);
    pthread_attr_setdetachstate(&thread_attr, PTHREAD_CREATE_JOINABLE);
    pthread_create(&capture_thread, &thread_attr, (void *(*)(void *))capture_func, c);
}


int main(int argc, char **argv)
{
    const char *stype = NULL;
    const char *target = NULL;
    const char *outdir = NULL;
    capture_type_t type;
    capture_flags_t flags = CAPTURE_FLAGS_NONE;

#ifdef PROFILING
    flags |= CAPTURE_FLAGS_NOWAIT;
#endif

    if (argc != 4)
    {
        fprintf(stderr, "invalid command line arguments\n\n");
        print_app_usage();
        return EXIT_FAILURE;
    }
    
    stype = argv[1];
    target = argv[2];
    outdir = argv[3];

    if (0 == strcmp(stype, "--file"))
        type = CAPTURE_FILE;
    else if (0 == strcmp(stype, "--iface"))
        type = CAPTURE_IFACE;
    else
    {
        fprintf(stderr, "invalid argument\n\n");
        print_app_usage();
        return EXIT_FAILURE;
    }

    g_stats = stats_create(outdir, NULL);
    if (!g_stats)
    {
        fprintf(stderr, "Unable to create stats\n");
        return EXIT_FAILURE;
    }
    g_capture = capture_create(type, target, g_stats, flags);
    if (!g_capture)
    {
        stats_destroy(g_stats);
        fprintf(stderr, "Unable to create capture\n");
        return EXIT_FAILURE;
    }
    
    if (!sig_setup())
    {
        capture_destroy(g_capture);
        stats_destroy(g_stats);
        return EXIT_FAILURE;
    }
    
#ifdef PROFILING
    if (type != CAPTURE_FILE)
#endif
    if (!create_stats_thread(g_stats))
    {
        capture_destroy(g_capture);
        stats_destroy(g_stats);
        return EXIT_FAILURE;
    }

    create_capture_thread(g_capture);
    
#ifdef PROFILING
    if (type == CAPTURE_FILE)
        pthread_join(capture_thread, NULL);
#endif

    stats_start_read(g_stats);
    
    fprintf(stderr, "FINISHING...\n");
    
#ifdef PROFILING
    if (type != CAPTURE_FILE)
#endif
    set_stats_val(STATS_EXIT);

#ifdef PROFILING
    if (type != CAPTURE_FILE)
    {
#endif
    pthread_join(stats_thread, NULL);
    pthread_join(capture_thread, NULL);
#ifdef PROFILING
    }
    else
        stats_save(g_stats);
#endif

    capture_destroy(g_capture);
    stats_destroy(g_stats);

    return EXIT_SUCCESS;
}
