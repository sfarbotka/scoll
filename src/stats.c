//
//  stats.c
//  scoll
//
//  Created by Sergey Farbotka on 9/24/14.
//  Copyright (c) 2014 Sergey Farbotka. All rights reserved.
//

#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <pthread.h>
#include <unistd.h>
#include "stats.h"
#include "debug.h"
#include "mpool.h"
#include "btree.h"

#define FNAME_POSTFIX_MAX_LEN (13 + 4)
#define OUTDIR_LEN 0x100
#define TIMEBUF_LEN 20
#define STATS_RECORD_QUEUE_ITEM_COUNT 10
#define STATS_MIN_QUEUE_TO_READ 100

typedef struct stats_user
{
    FILE*       f;
    u_long      num_packets;
} stats_user_t;

typedef struct stats_item
{
    u_long         num_bytes;
    u_long         num_packets;
} stats_item_t;

typedef struct stats_record_item
{
    stats_record_t              buf[STATS_RECORD_QUEUE_ITEM_COUNT];
    size_t                      count;
    struct stats_record_item*   next;
}stats_record_item_t;

struct stats
{
    pthread_mutex_t    tree_mutex;
    btree_t*           tree;

#ifdef PROFILING
    clock_t            add_clock;
    size_t             add_count;
    size_t             queue_nrecs_max;
    clock_t            stats_queue_clock;
    size_t             stats_queue_count;
#endif
    
    int                     id;

    pthread_mutex_t         queue_mutex;
    pthread_cond_t          queue_cond;
    mpool_t*                queue_pool;
    stats_record_item_t*    queue_head;
    stats_record_item_t*    queue_tail;
    int                     queue_finishing;
    size_t                  queue_nrecs;

    size_t                  num_packets;

    char*                   fname;
    char*                   fname_postfix;
};

static const char* proto2str(char* buf, u_short proto, size_t bufsize);

static int cmp_keys(const void* k1, const void* item);
static void write_item(FILE* f, const stats_key_t* key, const stats_item_t* item);
static void write_header(FILE* f);
static void add_postfix(char* buf);
static void add_stats(stats_t* s, const stats_record_t* srec);
static void foreach_node(btree_t* t, const stats_key_t* key, stats_item_t* data,
        stats_user_t* user);


const char* proto2str(char* buf, u_short proto, size_t bufsize)
{
    const char* p;
    switch (proto)
    {
        case IPPROTO_IP:
            p = "IP";
            break;
        case IPPROTO_TCP:
            p ="TCP";
            break;
        case IPPROTO_UDP:
            p = "UDP";
            break;
        case IPPROTO_ICMP:
            p = "ICMP";
            break;
        default:
            snprintf(buf, bufsize - 1, "%#x", proto);
            return buf;
    }

    strncpy(buf, p, bufsize - 1);
    return buf;
}

int cmp_keys(const void* k1, const void* k2)
{
    return memcmp(k1, k2, sizeof(stats_key_t));
}

void foreach_node(btree_t* t, const stats_key_t* key, stats_item_t* data,
        stats_user_t* user)
{
    user->num_packets += data->num_packets;
    write_item(user->f, key, data);
}

void write_item(FILE* f, const stats_key_t* key, const stats_item_t* item)
{
    char proto_buf[10];
    char src_buf[20];
    char dst_buf[20];

    fprintf(f, "%s,%lu,%lu,%s,%s,%d,%d\n",
            proto2str(proto_buf, key->proto, 10),
            item->num_packets,
            item->num_bytes,
            inet_ntop(AF_INET, &key->src_ip, src_buf, 20),
            inet_ntop(AF_INET, &key->dst_ip, dst_buf, 20),
            key->src_port,
            key->dst_port);
}


void write_header(FILE* f)
{
    fprintf(f, "PROTOCOL,PACKETS,BYTES,SRC_IP,DST_IP,SRC_PORT,DST_PORT\n");
}


void add_postfix(char* buf)
{
    time_t t = time(0);
    strftime(buf, 18, "%Y%m%d_%H%M.csv", localtime(&t));
}

void add_stats(stats_t* s, const stats_record_t* srec)
{
    stats_item_t* p;
    int ins;

#ifdef PROFILING
    clock_t c = clock();
#endif


    pthread_mutex_lock(&s->tree_mutex);
    
    ins = 1;
    p = (stats_item_t*)btree_search(s->tree, &srec->k, cmp_keys, &ins);
    if (ins)
    {
        p->num_bytes = srec->num_bytes;
        p->num_packets = 1;
    }
    else
    {
        p->num_bytes += srec->num_bytes;
        p->num_packets++;
    }

    //debug_log("tree size = %zu\n", btree_num_nodes(s->tree));
    pthread_mutex_unlock(&s->tree_mutex);

#ifdef PROFILING
    c = clock() - c;
    s->add_clock += c;
    s->add_count++;
#endif
}


stats_t* stats_create(const char* outdir, const char* prefix)
{
    static int create_index = 0;
    stats_t* s = (stats_t*)calloc(1, sizeof(stats_t));
    if (!s)
        return NULL;
    
    s->id = create_index++;

    // alloc fname
    const char* _prefix = (prefix ? prefix : "traffic-");
    
    size_t outdirlen = strlen(outdir);
    size_t fnamelen = outdirlen + strlen(_prefix) + FNAME_POSTFIX_MAX_LEN + 5;
    s->fname = calloc(fnamelen, 1);
    strcpy(s->fname, outdir);
    if (s->fname[outdirlen - 1] != '/')
    {
        s->fname[outdirlen] = '/';
        s->fname[outdirlen + 1] = 0;
    }
    strcat(s->fname, _prefix);
    s->fname_postfix = s->fname + strlen(s->fname);
    
    // init other things
    pthread_mutex_init(&s->tree_mutex, NULL);
    pthread_mutex_init(&s->queue_mutex, NULL);
    pthread_cond_init(&s->queue_cond, NULL);
    
    s->queue_finishing = 0;
    s->queue_pool = mpool_create(sizeof(stats_record_item_t));
    s->queue_head = NULL;
    s->tree = btree_create(sizeof(stats_key_t), sizeof(stats_item_t));

    debug_log("stats-%02x: stats_create(), ptr=%p.\n", s->id, s);

    return s;
}

void stats_queue(stats_t* s, const stats_record_t* srec)
{
#ifdef PROFILING
    clock_t c = clock();
#endif

    pthread_mutex_lock(&s->queue_mutex);

    if (!s->queue_head || (s->queue_tail->count == STATS_RECORD_QUEUE_ITEM_COUNT))
    {
        stats_record_item_t* p = (stats_record_item_t*)mpool_alloc(s->queue_pool);
        if (!s->queue_head)
            s->queue_head = p;
        else
            s->queue_tail->next = p;
        s->queue_tail = p;
        p->next = NULL;
        p->count = 0;
    }
    s->queue_tail->buf[s->queue_tail->count++] = *srec;
    s->queue_nrecs++;
#ifdef PROFILING
    if (s->queue_nrecs_max < s->queue_nrecs)
        s->queue_nrecs_max = s->queue_nrecs;
#endif

    if (s->queue_nrecs >= STATS_MIN_QUEUE_TO_READ)
        pthread_cond_signal(&s->queue_cond);

    pthread_mutex_unlock(&s->queue_mutex);

#ifdef PROFILING
    c = clock() - c;
    s->stats_queue_clock += c;
    s->stats_queue_count++;
#endif
}


void stats_start_read(stats_t* s)
{
    stats_record_item_t* queue_head = NULL;
    mpool_t* queue_pool = NULL;
    int finishing = 0;
    
    debug_log("stats-%02x: stats_start_read().\n", s->id);

    do
    {
        pthread_mutex_lock(&s->queue_mutex);
        while (1)
        {
            if (s->queue_head)
            {
                queue_head = s->queue_head;
                s->queue_head = NULL;
                s->queue_tail = NULL;
                queue_pool = s->queue_pool;
                s->queue_pool = mpool_create(sizeof(stats_record_item_t));
                s->queue_nrecs = 0;
                break;
            }
            if (s->queue_finishing)
                break;
            pthread_cond_wait(&s->queue_cond, &s->queue_mutex);
        }
        finishing = s->queue_finishing;
        pthread_mutex_unlock(&s->queue_mutex);
        
        if (queue_head)
        {
            stats_record_item_t* next;
            for (; queue_head; queue_head = next)
            {
                for (size_t i = 0; i < queue_head->count; i++)
                    add_stats(s, &queue_head->buf[i]);
                next = queue_head->next;
                mpool_free(queue_pool, queue_head);
            }
            mpool_destroy(queue_pool);
        }
    } while(!finishing);
    
    debug_log("stats-%02x: stats_start_read() finished.\n", s->id);
}

void stats_save(stats_t* s)
{
    int exists;
    btree_t* tree = NULL;

    debug_log("stats-%02x: stats_save().\n", s->id);

    // get collected stats
    pthread_mutex_lock(&s->tree_mutex);
    if (btree_num_nodes(s->tree))
    {
        tree = s->tree;
        s->tree = btree_create(sizeof(stats_key_t), sizeof(stats_item_t));
    }

    pthread_mutex_unlock(&s->tree_mutex);
    
    if (!tree)
    {
        debug_log("stats-%02x: no data to write\n", s->id);
        return;
    }

    // save collected stats
    add_postfix(s->fname_postfix);
    
    exists = (access(s->fname, F_OK) != -1);

    FILE* f = fopen(s->fname, "a+");
    if (f)
    {
        if (!exists)
            write_header(f);

        stats_user_t user;
        user.f = f;
        user.num_packets = 0;

        btree_foreach(tree, (btree_callback_t)foreach_node, &user);
        btree_destroy(tree);
        
        fclose(f);

        debug_log("stats-%02x: FILE '%s',  TOTAL PACKETS: %lu\n", s->id,
                s->fname, user.num_packets);
        s->num_packets += user.num_packets;
    }
    else
    {
        fprintf(stderr, "stats-%02x: Unable to create/open file: %s\n",
                s->id, s->fname);
    }
}

void stats_stop(stats_t* s)
{
    debug_log("stats-%02x: stats_stop().\n", s->id);
    pthread_mutex_lock(&s->queue_mutex);
    
    s->queue_finishing = 1;
    
    pthread_cond_signal(&s->queue_cond);
    pthread_mutex_unlock(&s->queue_mutex);
}

void stats_destroy(stats_t* s)
{
    debug_log("stats-%02x: stats_destroy().\n", s->id);

#ifdef PROFILING
    fprintf(stderr, "stats-%02x: PROFILING DATA:\n", s->id);
    fprintf(stderr, "stats-%02x: total num packets: %zu\n", s->id,
            s->num_packets);

    // queue and tree
    fprintf(stderr, "stats-%02x: %s\n", s->id,
            profiling2str("stats_queue()", s->stats_queue_clock,
                    s->stats_queue_count, s->num_packets));
    fprintf(stderr, "stats-%02x: stats_queue() max nrecs: %zu\n", s->id,
            s->queue_nrecs_max);
    fprintf(stderr, "stats-%02x: ---\n", s->id);

    // add_stats
    fprintf(stderr, "stats-%02x: %s\n", s->id,
            profiling2str("add_stats()", s->add_clock,
                    s->add_count, s->add_count));
    fprintf(stderr, "stats-%02x: ---\n", s->id);

    fprintf(stderr, "stats-%02x: PROFILING DATA ENDED\n", s->id);
#endif

    btree_destroy(s->tree);
    mpool_destroy(s->queue_pool);
    free(s->fname);

    free(s);
}

