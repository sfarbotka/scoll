//
//  stats.h
//  scoll
//
//  Created by Sergey Farbotka on 9/24/14.
//  Copyright (c) 2014 Sergey Farbotka. All rights reserved.
//

#ifndef __scoll__stats__
#define __scoll__stats__

#include <stdio.h>
#include <netinet/in.h>
#include <sys/types.h>

typedef struct stats_key
{
    u_short        proto;
    struct in_addr src_ip;
    struct in_addr dst_ip;
    u_short        src_port;
    u_short        dst_port;
} stats_key_t;

typedef struct stats_record
{
    stats_key_t k;
    u_short        num_bytes;
} stats_record_t;


typedef struct stats stats_t;

stats_t* stats_create(const char* outdir, const char* prefix);
void stats_queue(stats_t* s, const stats_record_t* srec);
void stats_start_read(stats_t* s);
void stats_save(stats_t* s);
void stats_stop(stats_t* s);
void stats_destroy(stats_t* s);


#endif /* defined(__scoll__stats__) */
