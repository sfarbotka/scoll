//
//  timer.c
//  scoll
//
//  Created by Sergey Farbotka on 9/24/14.
//  Copyright (c) 2014 Sergey Farbotka. All rights reserved.
//

#include <sys/time.h>
#include <stdio.h>
#include <signal.h>
#include <string.h>
#include "timer.h"


static timer_handler_t timer_handler = 0;

static void sigalrm_handler(int sig);


void sigalrm_handler(int sig)
{
    timer_handler();
}


int timer_setup(timer_handler_t handler)
{
    timer_handler = handler;
    
    struct sigaction alarm_act;
    
    bzero(&alarm_act, sizeof(struct sigaction));
    alarm_act.sa_handler=&sigalrm_handler;
    
    if (0 != sigaction(SIGALRM, &alarm_act, NULL))
    {
        fprintf(stderr, "sigaction(SIGALRM) failed\n");
        return 0;
    }
    
    return 1;
}

void timer_arm_once(int seconds)
{
    struct itimerval alarm_timer;

    alarm_timer.it_value.tv_sec = seconds;
    alarm_timer.it_value.tv_usec = 0;
    alarm_timer.it_interval.tv_sec = 0;
    alarm_timer.it_interval.tv_usec = 0;
    
    setitimer(ITIMER_REAL, &alarm_timer, NULL);
}
