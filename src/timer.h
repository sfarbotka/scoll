//
//  timer.h
//  scoll
//
//  Created by Sergey Farbotka on 9/24/14.
//  Copyright (c) 2014 Sergey Farbotka. All rights reserved.
//

#ifndef __scoll__timer__
#define __scoll__timer__

typedef void (*timer_handler_t)(void);

int timer_setup(timer_handler_t handler);
void timer_arm_once(int seconds);


#endif /* defined(__scoll__timer__) */
