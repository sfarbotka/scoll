/*
 * check_addons.h
 *
 *  Created on: Oct 7, 2014
 *      Author: fsv
 */

#ifndef CHECK_ADDONS_H_
#define CHECK_ADDONS_H_

#include <stdio.h>
#include <check.h>

#define _ck_assert_size(X, OP, Y) do { \
  size_t _ck_x = (X); \
  size_t _ck_y = (Y); \
  ck_assert_msg(_ck_x OP _ck_y, "Assertion '%s' failed: %s==%ju, %s==%ju", #X#OP#Y, #X, _ck_x, #Y, _ck_y); \
} while (0)

#define ck_assert_size_eq(X, Y) _ck_assert_size(X, ==, Y)
#define ck_assert_size_ne(X, Y) _ck_assert_size(X, !=, Y)
#define ck_assert_size_lt(X, Y) _ck_assert_size(X, <, Y)
#define ck_assert_size_le(X, Y) _ck_assert_size(X, <=, Y)
#define ck_assert_size_gt(X, Y) _ck_assert_size(X, >, Y)
#define ck_assert_size_ge(X, Y) _ck_assert_size(X, >=, Y)

#endif /* CHECK_ADDONS_H_ */
