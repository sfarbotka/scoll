#include <stdlib.h>
#include <check.h>
#include "check_addons.h"

#include "../src/debug.c"
#include "../src/mpool.c"
#include "../src/bstack.c"

bstack_t* stack;

START_TEST(core_create_destroy)
{
    bstack_t* s = bstack_create(sizeof(int));
    bstack_destroy(s);
}
END_TEST

START_TEST(core_create_destroy_nonempty)
{
    bstack_t* s = bstack_create(sizeof(int));
    bstack_push(s);
    bstack_destroy(s);
}
END_TEST

static void _stack_setup(void)
{
    stack = bstack_create(sizeof(int));
}

static void _stack_teardown(void)
{
    bstack_destroy(stack);
    stack = NULL;
}

START_TEST(stack_size)
{
    ck_assert_size_eq(bstack_size(stack), 0);

    for (size_t i = 0; i < 10; i++)
    {
        ck_assert_ptr_ne(bstack_push(stack), NULL);
        ck_assert_size_eq(bstack_size(stack), i + 1);
    }

    for (size_t i = bstack_size(stack);i > 0;i--)
    {
        ck_assert_int_ne(bstack_pop(stack), 0);
        ck_assert_size_eq(bstack_size(stack), i - 1);
    }
}
END_TEST

START_TEST(stack_pop_empty)
{
    ck_assert_int_eq(bstack_pop(stack), 0);
}
END_TEST

START_TEST(stack_data)
{
    for (size_t i = 0; i < 10; i++)
    {
        int* p = (int*)bstack_push(stack);
        *p = i;
    }

    for (size_t i = bstack_size(stack);i > 0;i--)
    {
        int* p = bstack_last(stack);
        ck_assert_int_eq(*p, i - 1);
        bstack_pop(stack);
    }
}
END_TEST


START_TEST(stack_last)
{
    ck_assert_ptr_eq(bstack_last(stack), NULL);

    for (size_t i = 0; i < 10; i++)
    {
        int* p = (int*)bstack_push(stack);
        ck_assert_ptr_eq(bstack_last(stack), p);
    }
}
END_TEST

START_TEST(stack_clear)
{
    for (size_t i = 0; i < 10; i++)
        bstack_push(stack);

    bstack_clear(stack);
    ck_assert_size_eq(bstack_size(stack), 0);
    ck_assert_ptr_eq(bstack_last(stack), NULL);
}
END_TEST

Suite* create_bstack_suite(void)
{
    Suite* s = suite_create("bstack");

    TCase* tcore = tcase_create("core");
    tcase_add_test(tcore, core_create_destroy);
    tcase_add_test(tcore, core_create_destroy_nonempty);
    suite_add_tcase(s, tcore);

    TCase* tstack = tcase_create("stack");
    tcase_add_checked_fixture(tstack, _stack_setup, _stack_teardown);
    tcase_add_test(tstack, stack_size);
    tcase_add_test(tstack, stack_last);
    tcase_add_test(tstack, stack_data);
    tcase_add_test(tstack, stack_pop_empty);
    tcase_add_test(tstack, stack_clear);
    suite_add_tcase(s, tstack);

    return s;
}

int main(void)
{
    Suite* s = create_bstack_suite();
    SRunner* sr = srunner_create(s);

    srunner_run_all(sr, CK_VERBOSE);
    int number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);

    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}



