/*
 * test_mpoll.c
 *
 *  Created on: Oct 6, 2014
 *      Author: fsv
 */

#include <stdlib.h>
#include <check.h>
#include "check_addons.h"

#include "../src/debug.c"
#include "../src/mpool.c"

#define TESTS_DATA_SIZE 4

mpool_t* pool;
mpool_t* pool1;
mpool_t* pool2;

static size_t get_heaps_count(mpool_t* p)
{
    size_t i = 0;
    mpool_heap_t* h = pool->heap_head;

    for (;h; h = h->next)
        i++;

    return i;
}

static size_t get_blocks_count(mpool_block_t* bhead)
{
    size_t i = 0;
    mpool_block_t* b = bhead;

    for (;b; b = b->next)
        i++;

    return i;
}

static void check_blocks_linked_list(mpool_block_t* bhead, int allocated)
{
    mpool_block_t* bprev = NULL;
    mpool_block_t* bcurr = bhead;

    for (; bcurr; bprev = bcurr, bcurr = bcurr->next)
    {
        ck_assert_int_eq(bcurr->allocated, allocated);
        ck_assert_ptr_eq(bcurr->prev, bprev);
        if (bprev)
            ck_assert_ptr_eq(bprev->next, bcurr);
    }
    if (bprev)
        ck_assert_ptr_eq(bprev->next, NULL);
}


START_TEST(core_create_destroy)
{
    mpool_t* p = mpool_create(TESTS_DATA_SIZE);
    mpool_destroy(p);
}
END_TEST

START_TEST(core_limits)
{
    mpool_t* p = mpool_create(0);
    ck_assert_ptr_eq(p, NULL);
}
END_TEST

START_TEST(core_size)
{
    mpool_t* p = mpool_create(TESTS_DATA_SIZE);
    ck_assert(p->data_size == TESTS_DATA_SIZE);
    ck_assert(p->block_size == (TESTS_DATA_SIZE + sizeof(mpool_block_t)));
    mpool_destroy(p);
}
END_TEST


static void _heap_setup(void)
{
    pool = mpool_create(TESTS_DATA_SIZE);
}

static void _heap_teardown(void)
{
    mpool_destroy(pool);
    pool = NULL;
}

START_TEST(heap_check_1)
{
    mpool_heap_t* h = pool->heap_head;
    ck_assert_ptr_ne(h, NULL);
    ck_assert_ptr_eq(h->prev, NULL);
    ck_assert_ptr_eq(h->next, NULL);

    ck_assert_size_eq(get_heaps_count(pool), 1);
    ck_assert_size_eq(pool->blocks_total_count, h->blocks_count);
}
END_TEST

START_TEST(heap_check_2)
{
    heap_add(pool);
    mpool_heap_t* h = pool->heap_head;
    ck_assert_ptr_ne(h, NULL);
    ck_assert_ptr_eq(h->prev, NULL);
    mpool_heap_t* hn = h->next;
    ck_assert_ptr_ne(hn, NULL);
    ck_assert_ptr_eq(hn->prev, h);
    ck_assert_ptr_eq(hn->next, NULL);

    ck_assert_size_eq(get_heaps_count(pool), 2);
    ck_assert_size_eq(pool->blocks_total_count,
            (h->blocks_count + hn->blocks_count));
}
END_TEST

START_TEST(heap_check_blocks_size)
{
    mpool_heap_t* h = pool->heap_head;
    mpool_block_t* b1 = h->bhead;
    mpool_block_t* b2 = b1->next;
    ck_assert_size_eq(((char*)b2) - ((char*)b1), h->block_size);
}
END_TEST

START_TEST(heap_check_blocks_linked_list)
{
    check_blocks_linked_list(pool->bhead_alloc, 1);
    check_blocks_linked_list(pool->bhead_free, 0);
    heap_add(pool);
    check_blocks_linked_list(pool->bhead_alloc, 1);
    check_blocks_linked_list(pool->bhead_free, 0);
}
END_TEST

START_TEST(heap_check_blocks_2)
{
    ck_assert_size_eq(get_blocks_count(pool->bhead_free), HEAP_SIZE_MIN);
    ck_assert_size_eq(get_blocks_count(pool->bhead_alloc), 0);
}
END_TEST

static void _pool_setup(void)
{
    pool = mpool_create(TESTS_DATA_SIZE);
    pool1 = mpool_create(TESTS_DATA_SIZE);
    pool2 = mpool_create(TESTS_DATA_SIZE);
}

static void _pool_teardown(void)
{
    mpool_destroy(pool2);
    mpool_destroy(pool1);
    mpool_destroy(pool);
    pool1 = NULL;
    pool2 = NULL;
    pool = NULL;
}

START_TEST(pool_alloc_free)
{
    void* parray[HEAP_SIZE_MIN + 1];
    size_t ca = 0;
    size_t cf = HEAP_SIZE_MIN;
    size_t i;

    // check alloc
    for (i = 0; i < HEAP_SIZE_MIN; i++)
    {
        parray[i] = mpool_alloc(pool);
        memset(parray[i], 0x10, TESTS_DATA_SIZE);
        ca++;
        cf--;
        ck_assert_int_eq(ptr2block(parray[i])->allocated, 1);
        ck_assert_size_eq(get_blocks_count(pool->bhead_alloc), ca);
        ck_assert_size_eq(get_blocks_count(pool->bhead_free), cf);
    }

    // check add heap
    parray[i] = mpool_alloc(pool);
    ca++;
    ck_assert_size_eq(get_heaps_count(pool), 2);
    ck_assert_size_eq(get_blocks_count(pool->bhead_alloc), ca);

    check_blocks_linked_list(pool->bhead_free, 0);
    check_blocks_linked_list(pool->bhead_alloc, 1);

    // check free
    cf = get_blocks_count(pool->bhead_free);
    for (i = 0; i < HEAP_SIZE_MIN + 1; i++)
    {
        mpool_free(pool, parray[i]);

        ca--;
        cf++;

        ck_assert_int_eq(ptr2block(parray[i])->allocated, 0);
        ck_assert_size_eq(get_blocks_count(pool->bhead_alloc), ca);
        ck_assert_size_eq(get_blocks_count(pool->bhead_free), cf);
    }
    ck_assert_size_eq(get_blocks_count(pool->bhead_alloc), 0);

    check_blocks_linked_list(pool->bhead_free, 0);
    check_blocks_linked_list(pool->bhead_alloc, 1);
}
END_TEST


Suite* create_mpool_suite(void)
{
    Suite* s = suite_create("mpool");

    TCase* tccore = tcase_create("core");
    tcase_add_test(tccore, core_create_destroy);
    tcase_add_test(tccore, core_limits);
    tcase_add_test(tccore, core_size);
    suite_add_tcase(s, tccore);

    TCase* tcheap = tcase_create("heap");
    tcase_add_checked_fixture(tcheap, _heap_setup, _heap_teardown);
    tcase_add_test(tcheap, heap_check_1);
    tcase_add_test(tcheap, heap_check_2);
    tcase_add_test(tcheap, heap_check_blocks_size);
    tcase_add_test(tcheap, heap_check_blocks_linked_list);
    tcase_add_test(tcheap, heap_check_blocks_2);
    suite_add_tcase(s, tcheap);

    TCase* tcpool = tcase_create("pool");
    tcase_add_checked_fixture(tcpool, _pool_setup, _pool_teardown);
    tcase_add_test(tcpool, pool_alloc_free);
    suite_add_tcase(s, tcpool);

    return s;
}

int main(void)
{
    Suite* s = create_mpool_suite();
    SRunner* sr = srunner_create(s);

    srunner_run_all(sr, CK_VERBOSE);
    int number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);

    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
